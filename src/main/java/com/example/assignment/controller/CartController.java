package com.example.assignment.controller;

import com.example.assignment.domain.Cart;
import com.example.assignment.domain.Order;
import com.example.assignment.service.CartService;
import com.example.assignment.service.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/cart")
@CrossOrigin("*")
public class CartController {
    private final CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @PostMapping("/get-all-cart")
    public ResponseEntity<ResponseDTO> getAllCart(@RequestBody CartDTO dto, Pageable pageable) {
        Page<CartDTO> result = cartService.getAllCart(dto, pageable);
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<ResponseDTO> saveInCart(@RequestBody CartDTO dto) {
        CartDTO result = cartService.saveInCart(dto);
        // check user_id and product_id is present ? update quality : save new
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<ResponseDTO> deleteFromCart(@RequestBody CartDTO dto) {
        CartDTO result = cartService.deleteFromCart(dto);
        // check quality new === quality old ? delete : update quality = old - new
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }

    @PostMapping("/buy-product")
    public ResponseEntity<ResponseDTO> buyProduct(@RequestBody CartProductDTO dto) {
        Order result = cartService.buyProduct(dto);
        // save into order and order detail
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }
}
