package com.example.assignment.controller;

import com.example.assignment.service.ProductService;
import com.example.assignment.service.dto.ProductDTO;
import com.example.assignment.service.dto.ResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api/product")
@CrossOrigin("*")

public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/all")
    public ResponseEntity<ResponseDTO> getAllProduct(@RequestBody ProductDTO dto, Pageable pageable){
        Page<ProductDTO> result = productService.getAllProduct(dto, pageable);
        return new ResponseEntity<>(ResponseDTO.success(result),HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<ResponseDTO> saveProduct(@RequestBody ProductDTO dto){
        ProductDTO result = productService.saveProduct(dto);
        return new ResponseEntity<>(ResponseDTO.success(result),HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<ResponseDTO> deleteProduct(@RequestBody ProductDTO dto){
        ProductDTO result = productService.deleteProduct(dto);
        return new ResponseEntity<>(ResponseDTO.success(result),HttpStatus.OK);
    }


}
