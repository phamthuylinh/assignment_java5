package com.example.assignment.controller;

import com.example.assignment.domain.Category;
import com.example.assignment.service.CategoryService;
import com.example.assignment.service.dto.ResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/category")
@CrossOrigin("*")

public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/get-category")
    public ResponseEntity<ResponseDTO> getCategory() {
        List<Category> result = categoryService.getCategory();
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<ResponseDTO> saveCategory(@RequestBody Category category) {
        Category result = categoryService.saveCategory(category);
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<ResponseDTO> deleteCategory(@RequestBody Category category) {
        Category result = categoryService.deleteCategory(category);
        // set enable = false
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }
}
