package com.example.assignment.controller;

import com.example.assignment.service.OrderService;
import com.example.assignment.service.dto.OrderDTO;
import com.example.assignment.service.dto.ResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/order")
@CrossOrigin("*")

public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/get-order")
    public ResponseEntity<ResponseDTO> getOrder(@RequestBody OrderDTO dto, Pageable pageable) {
        Page<OrderDTO> result = orderService.getOrder(dto, pageable);
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }

    @PostMapping("/order-detail")
    public ResponseEntity<ResponseDTO> getOrderDetail(@RequestBody OrderDTO dto, Pageable pageable) {
        Page<OrderDTO> result = orderService.getOrderDetail(dto, pageable);
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }
}
