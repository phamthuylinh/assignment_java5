package com.example.assignment.controller;

import com.example.assignment.service.UserService;
import com.example.assignment.service.dto.ResponseDTO;
import com.example.assignment.service.dto.UserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
@CrossOrigin("*")

public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<ResponseDTO> getUser(@RequestBody UserDTO dto){
        UserDTO result = userService.getUserLogin(dto);
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }

    @PostMapping("/change-password")
    public ResponseEntity<ResponseDTO> changePassword(@RequestBody UserDTO dto){
        UserDTO result = userService.changePassword(dto);
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<ResponseDTO> createUser(@RequestBody UserDTO dto){
        UserDTO result = userService.createUser(dto);
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }

    @PostMapping("/forgot-pass")
    public ResponseEntity<ResponseDTO> forgotPass(@RequestBody UserDTO dto){
        UserDTO result = userService.forgotPass(dto);
        return new ResponseEntity<>(ResponseDTO.success(result), HttpStatus.OK);
    }

}
