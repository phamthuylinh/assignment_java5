package com.example.assignment.service;

import com.example.assignment.repository.OrderRepository;
import com.example.assignment.service.dto.OrderDTO;
import com.example.assignment.service.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class OrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Page<OrderDTO> getOrder(OrderDTO dto, Pageable pageable) {
        return orderRepository.getAllOrder(dto,pageable);
    }

    public Page<OrderDTO> getOrderDetail(OrderDTO dto, Pageable pageable) {
        return orderRepository.getOrderDetail(dto, pageable);
    }
}
