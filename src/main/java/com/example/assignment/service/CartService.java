package com.example.assignment.service;

import com.example.assignment.config.exception.PermissionDeniedException;
import com.example.assignment.config.exception.ProductNotFoundException;
import com.example.assignment.config.exception.UserNotFoundException;
import com.example.assignment.domain.Cart;
import com.example.assignment.domain.Order;
import com.example.assignment.domain.OrderDetail;
import com.example.assignment.domain.Product;
import com.example.assignment.repository.*;
import com.example.assignment.service.dto.CartDTO;
import com.example.assignment.service.dto.CartProductDTO;
import com.example.assignment.service.dto.ProductDTO;
import com.example.assignment.service.dto.UserDTO;
import com.example.assignment.service.mapper.MapperUtils;
import com.example.assignment.service.utils.DataUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Service
@Transactional
public class CartService {

    private final CartRepository cartRepository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final OrderRepository orderRepository;
    private final OrderDetailRepository orderDetailRepository;

    public CartService(CartRepository cartRepository, ProductRepository productRepository, UserRepository userRepository, OrderRepository orderRepository, OrderDetailRepository orderDetailRepository) {
        this.cartRepository = cartRepository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.orderRepository = orderRepository;
        this.orderDetailRepository = orderDetailRepository;
    }

    public Page<CartDTO> getAllCart(CartDTO dto, Pageable pageable) {
        return cartRepository.getAllCart(dto, pageable);
    }

    public CartDTO saveInCart(CartDTO dto) {
        userRepository.findById(dto.getUserId()).orElseThrow(UserNotFoundException::new);
//        Cart cart = new Cart();
        ProductDTO productDTO = dto.getProduct();

        // check quality and price
        Product product = productRepository.findById(productDTO.getId()).orElseThrow(ProductNotFoundException::new);
        if (product.getQuality() <= 0 || !productDTO.getPrice().equals(product.getPrice())) {
            throw new PermissionDeniedException();
        }

        Cart cart = cartRepository.findCartByUserIdAndProductId(dto.getUserId(), product.getId());
        if (!DataUtils.isNullObject(cart)) {
            cart.setQuality(cart.getQuality() + productDTO.getQuality());
            cart.setPrice(productDTO.getPrice());
            product.setQuality(product.getQuality() - productDTO.getQuality());
            cart.setCreateTimestamp(Instant.now());
            cart = cartRepository.save(cart);
        } else {
            Cart cart1 =  Cart.builder().price(productDTO.getPrice())
                    .quality(productDTO.getQuality())
                    .productId(productDTO.getId())
                    .userId(dto.getUserId())
                    .build();
            product.setQuality(product.getQuality() - productDTO.getQuality());
            cart1.setCreateTimestamp(Instant.now());

            cart = cartRepository.save(cart1);
        }

        return new CartDTO(cart.getId(), cart.getUserId(), MapperUtils.map(productRepository.save(product), ProductDTO.class));
    }

    public CartDTO deleteFromCart(CartDTO dto) {
        userRepository.findById(dto.getUserId()).orElseThrow(UserNotFoundException::new);
        Cart cart = cartRepository.findById(dto.getId()).orElseThrow(ProductNotFoundException::new);
        ProductDTO productDTO = dto.getProduct();

        Product product = productRepository.findById(productDTO.getId()).orElseThrow(ProductNotFoundException::new);


        if (productDTO.getQuality() > cart.getQuality() || !productDTO.getPrice().equals(cart.getPrice())) {
            throw new PermissionDeniedException();
        }
        if (productDTO.getQuality().equals(cart.getQuality())) {
            product.setQuality(product.getQuality() + productDTO.getQuality());
            productRepository.save(product);
            cartRepository.delete(cart);
            return null;
        } else {
            cart.setQuality(cart.getQuality() - productDTO.getQuality());
            cart = cartRepository.save(cart);
            product.setQuality(product.getQuality() + productDTO.getQuality());
            return new CartDTO(cart.getId(), cart.getUserId(), MapperUtils.map(productRepository.save(product), ProductDTO.class));
        }
    }

    public Order buyProduct(CartProductDTO dto) {
        userRepository.findById(dto.getUserId()).orElseThrow(UserNotFoundException::new);

        Long sumQuality = 0L;
        Double sumPrice = 0D;

        for (CartDTO cartDTO : dto.getProducts()) {
            cartRepository.findById(cartDTO.getId()).orElseThrow(ProductNotFoundException::new);
            productRepository.findById(cartDTO.getProId()).orElseThrow(ProductNotFoundException::new);
            sumQuality += cartDTO.getQuality();
            sumPrice += cartDTO.getPrice() * cartDTO.getQuality();
        }

        Order order = Order.builder().userId(dto.getUserId())
                .sumPrice(sumPrice)
                .sumQuality(sumQuality)
                .createTimestamp(Instant.now())
                .build();

        Order finalOrder = orderRepository.save(order);
        for (CartDTO p : dto.getProducts()){
            Cart cart = cartRepository.findById(p.getId()).orElseThrow(ProductNotFoundException::new);

            OrderDetail orderDetail = OrderDetail.builder().orderId(finalOrder.getId())
                    .productId(p.getProId())
                    .price(p.getPrice())
                    .quality(p.getQuality())
                    .build();
            orderDetailRepository.save(orderDetail);
            cartRepository.delete(cart);
        };
        return finalOrder;
    }
}
