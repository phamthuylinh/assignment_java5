package com.example.assignment.service;

import com.example.assignment.config.exception.ProductNotFoundException;
import com.example.assignment.domain.Category;
import com.example.assignment.repository.CategoryRepository;
import com.example.assignment.service.utils.DataUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Category> getCategory() {
        return categoryRepository.findAll().stream()
                .filter(c -> c.getEnable())
                .collect(Collectors.toList());
    }

    public Category saveCategory(Category category) {
        if(!DataUtils.isNullObject(category.getId())){
            categoryRepository.findById(category.getId()).orElseThrow(ProductNotFoundException::new);
        }
        return categoryRepository.save(category);
    }

    public Category deleteCategory(Category category) {
        categoryRepository.findById(category.getId()).orElseThrow(ProductNotFoundException::new);
        category.setEnable(false);
        return categoryRepository.save(category);
    }
}
