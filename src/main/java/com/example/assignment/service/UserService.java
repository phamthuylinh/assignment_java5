package com.example.assignment.service;

import com.example.assignment.config.exception.UserDuplicateException;
import com.example.assignment.config.exception.UserNotFoundException;
import com.example.assignment.domain.User;
import com.example.assignment.repository.UserRepository;
import com.example.assignment.service.dto.UserDTO;
import com.example.assignment.service.mapper.MapperUtils;
import com.example.assignment.service.utils.DataUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDTO createUser(UserDTO dto) {

        if(dto.getId() == null && !checkDuplicateLogin(dto) && !checkDuplicateEmail(dto)){
            dto = MapperUtils.map(userRepository.save(MapperUtils.map(dto, User.class)),UserDTO.class);
        } else {
            User user = userRepository.findById(dto.getId()).orElseThrow(UserNotFoundException::new);

            if(!user.getEmail().equals(dto.getEmail()) && !checkDuplicateEmail(dto)){
                user.setEmail(dto.getEmail());
            }
            user.setName(dto.getName());
            user.setPhone(dto.getPhone());
            user.setAddress(dto.getAddress());
            user.setRoleId(dto.getRoleId());
            dto = MapperUtils.map(userRepository.save(user), UserDTO.class);
        }
        return dto;
    }


    public Boolean checkDuplicateLogin(UserDTO dto){
        return  (userRepository.findUserByLogin(dto.getLogin()) != null);
    }
    public Boolean checkDuplicateEmail(UserDTO dto){
        return  (userRepository.findUserByEmail(dto.getEmail()) != null);
    }


    public UserDTO changePassword(UserDTO dto) {
        User user = userRepository.findById(dto.getId()).orElseThrow(UserNotFoundException :: new);

        if(!user.getLogin().equals(dto.getLogin())){
            if(!checkDuplicateLogin(dto)){
                user.setLogin(dto.getLogin());
            }
        }
        if(!user.getPassword().equals(dto.getPassword())){
            user.setPassword(dto.getPassword());
        }

        return MapperUtils.map(userRepository.save(user), UserDTO.class);
    }

    public UserDTO getUserLogin(UserDTO dto) {
        User user = userRepository.findUserByLoginAndPassword(dto.getLogin(), dto.getPassword());
        if(DataUtils.isNullObject(user)){
            return null;
        } else{
            return MapperUtils.map(user, UserDTO.class);
        }
    }

    public UserDTO forgotPass(UserDTO dto) {
        User user = userRepository.findUserByLogin(dto.getLogin());

        if(DataUtils.isNullObject(user)){
            throw new UserNotFoundException();
        }

        user.setPassword("123@123a");
        return MapperUtils.map(user, UserDTO.class);

    }
}
