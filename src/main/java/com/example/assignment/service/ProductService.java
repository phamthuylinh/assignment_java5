package com.example.assignment.service;

import com.example.assignment.config.exception.ProductNotFoundException;
import com.example.assignment.domain.Cart;
import com.example.assignment.domain.Product;
import com.example.assignment.repository.CartRepository;
import com.example.assignment.repository.ProductRepository;
import com.example.assignment.service.dto.ProductDTO;
import com.example.assignment.service.mapper.MapperUtils;
import com.example.assignment.service.utils.DataUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProductService {

    private final ProductRepository productRepository;
    private final CartRepository cartRepository;

    public ProductService(ProductRepository productRepository, CartRepository cartRepository) {
        this.productRepository = productRepository;
        this.cartRepository = cartRepository;
    }

    public Page<ProductDTO> getAllProduct(ProductDTO dto, Pageable pageable) {

        return productRepository.getAllProduct(dto,pageable);
    }

    public ProductDTO saveProduct(ProductDTO dto) {
        if(DataUtils.isNullObject(dto.getId())){
            return MapperUtils.map(productRepository.save(MapperUtils.map(dto, Product.class)), ProductDTO.class);
        }else {
            Product product = MapperUtils.map(dto,Product.class);
            List<Cart>  list = cartRepository.findAllByProductId(product.getId())
                    .stream().peek(c -> c.setPrice(product.getPrice()))
                    .collect(Collectors.toList());
            cartRepository.saveAll(list);
            dto = MapperUtils.map(productRepository.save(product), ProductDTO.class);
        }
        return dto;
    }

    public ProductDTO deleteProduct(ProductDTO dto) {
        Product product = productRepository.findById(dto.getId()).orElseThrow(ProductNotFoundException::new);
        product.setEnable(!product.getEnable());

        return MapperUtils.map(productRepository.save(product), ProductDTO.class);
    }
}
