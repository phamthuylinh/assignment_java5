package com.example.assignment.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartDTO {
    private Long id;
    private Long userId;
    private ProductDTO product;
    private Long proId;
    private String proName;
    private Double price;
    private Long quality;
    private Instant createTimestamp;
    private String img;
    private Long maxQuality;

    public CartDTO(Long id, Long userId, ProductDTO product) {
        this.id = id;
        this.userId = userId;
        this.product = product;
    }

    public CartDTO(Long id, Long proId, String proName, Long quality, Double price, String img, Long userId, Instant createTimestamp,
                   Long maxQuality) {
        this.id = id;
        this.userId = userId;
        this.proId = proId;
        this.proName = proName;
        this.price = price;
        this.quality = quality;
        this.createTimestamp = createTimestamp;
        this.img = img;
        this.maxQuality = maxQuality;
    }
}
