package com.example.assignment.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {
    private Long id;
    private String userName;
    private Long userId;
    private Long sumQuality;
    private Double sumPrice;
    private Instant createTimestamp;
    private String productName;
    private Long productId;
    private Long qualityEach;
    private Double priceEach;
    private Long orderDetailId;

    //getAllOder
    public OrderDTO(Long id,Long userId, String userName,  Long sumQuality, Double sumPrice, Instant createTimestamp) {
        this.id = id;
        this.userName = userName;
        this.userId = userId;
        this.sumQuality = sumQuality;
        this.sumPrice = sumPrice;
        this.createTimestamp = createTimestamp;
    }

    //getOderDetail
    public OrderDTO(Long orderDetailId, Long id,  Long productId,String productName, Double priceEach, Long qualityEach) {
        this.orderDetailId=orderDetailId;
        this.id = id;
        this.productName = productName;
        this.productId = productId;
        this.qualityEach = qualityEach;
        this.priceEach = priceEach;
    }
}
