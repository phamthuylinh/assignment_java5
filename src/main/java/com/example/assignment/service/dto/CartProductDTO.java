package com.example.assignment.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CartProductDTO {
    private Long userId;
    private List<CartDTO> products;

}
