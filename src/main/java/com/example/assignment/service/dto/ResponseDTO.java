package com.example.assignment.service.dto;

import com.example.assignment.config.Constants;
import com.example.assignment.config.ErrorCode;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class ResponseDTO {
    private String code;
    private Object message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String detail;
    private Object response;
    private ErrorCode.Params[] params;

    public void setSuccess() {
        this.code = Constants.SUCCESS_CODE;
        this.message = Constants.SUCCESS_MSG;
    }

    public void setSuccess(Object response) {
        this.code = Constants.SUCCESS_CODE;
        this.message = Constants.SUCCESS_MSG;
        this.response = response;
    }

    public void setFail(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public void setFail() {
        this.code = Constants.FAIL_CODE;
        this.message = Constants.FAIL_MSG;
    }

    public static <T> ResponseDTO success(T data) {
        ResponseDTO res = new ResponseDTO();
        res.setCode(Constants.SUCCESS_CODE);
        res.setMessage(Constants.SUCCESS_MSG);
        res.setResponse(data);
        return res;
    }

    public static ResponseDTO success() {
        ResponseDTO res = new ResponseDTO();
        res.setCode(Constants.SUCCESS_CODE);
        res.setMessage(Constants.SUCCESS_MSG);
        return res;
    }

    public static ResponseDTO error() {
        ResponseDTO res = new ResponseDTO();
        res.setCode(Constants.FAIL_CODE);
        res.setMessage(Constants.FAIL_MSG);
        return res;
    }

    public static ResponseDTO error(String code, Object message) {
        ResponseDTO res = new ResponseDTO();
        res.setCode(code);
        res.setMessage(message);
        return res;
    }

    public static ResponseDTO error(String code, Object message, ErrorCode.Params[] args) {
        ResponseDTO res = error(code, message);
        res.setParams(args);
        return res;
    }

    public static ResponseDTO error(String code, Object message, String detail) {
        ResponseDTO res = new ResponseDTO();
        res.setCode(code);
        res.setMessage(message);
        res.setDetail(detail);
        return res;
    }
}
