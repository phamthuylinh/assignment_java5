package com.example.assignment.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO {
    private Long id;
    private String name;
    private Double price;
    private Long quality;
    private Long cateId;
    private String cateName;
    private String img;
    private Boolean enable;

    public ProductDTO(Long id, String name,Long cateId, String cateName,  String img , Double price, Long quality, Boolean enable) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quality = quality;
        this.cateId = cateId;
        this.cateName = cateName;
        this.img = img;
        this.enable = enable;
    }
}
