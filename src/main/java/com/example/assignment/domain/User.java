package com.example.assignment.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "`user`")
@Data
public class User {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "phone")
    private String phone;
    @Column(name = "email")
    private String email;
    @Column(name = "address")
    private String address;
    @Column(name = "role_id")
    private Long roleId;
    @Column(name = "password")
    private String password;
    @Column(name = "login")
    private String login;


}
