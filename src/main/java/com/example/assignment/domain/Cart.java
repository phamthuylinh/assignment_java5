package com.example.assignment.domain;

import com.example.assignment.service.dto.CartDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "`cart`")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@SqlResultSetMapping(
        name = "getListCart",
        classes = {
                @ConstructorResult(
                        targetClass = CartDTO.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "proId", type = Long.class),
                                @ColumnResult(name = "proName", type = String.class),
                                @ColumnResult(name = "quality", type = Long.class),
                                @ColumnResult(name = "price", type = Double.class),
                                @ColumnResult(name = "img", type = String.class),
                                @ColumnResult(name = "userId", type = Long.class),
                                @ColumnResult(name = "createTimestamp", type = Instant.class),
                                @ColumnResult(name = "maxQuality", type = Long.class)
                        }

                )
        }
)
public class Cart {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "product_id")
    private Long productId;
    @Column(name = "quality")
    private Long quality;
    @Column(name = "price")
    private Double price;
    @Column(name = "create_timestamp")
    private Instant createTimestamp;

}
