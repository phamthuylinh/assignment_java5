package com.example.assignment.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "`category`")
@Data
public class Category {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "enable")
    private Boolean enable;

}
