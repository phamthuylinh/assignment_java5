package com.example.assignment.domain;

import com.example.assignment.service.dto.OrderDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "`order`")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@SqlResultSetMapping(
        name = "getListOrder",
        classes = {
                @ConstructorResult(
                        targetClass = OrderDTO.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "userID", type = Long.class),
                                @ColumnResult(name = "userName", type = String.class),
                                @ColumnResult(name = "sumQuality", type = Long.class),
                                @ColumnResult(name = "sumPrice", type = Double.class),
                                @ColumnResult(name = "createTimestamp", type = Instant.class),

                        }
                )
        }
)
@SqlResultSetMapping(
        name = "getListOrderDetail",
        classes = {
                @ConstructorResult(
                        targetClass = OrderDTO.class,
                        columns = {
                                @ColumnResult(name = "orderDetailId", type = Long.class),
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "productId", type = Long.class),
                                @ColumnResult(name = "productName", type = String.class),
                                @ColumnResult(name = "priceEach", type = Double.class),
                                @ColumnResult(name = "qualityEach", type = Long.class),
                        }
                )
        }
)
public class Order {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "sum_quality")
    private Long sumQuality;
    @Column(name = "sum_price")
    private Double sumPrice;
    @Column(name = "create_timestamp")
    private Instant createTimestamp;

}
