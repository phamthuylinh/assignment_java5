package com.example.assignment.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "`role`")
@Data
public class Role {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;

}
