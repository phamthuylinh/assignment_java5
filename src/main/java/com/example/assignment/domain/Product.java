package com.example.assignment.domain;

import com.example.assignment.service.dto.ProductDTO;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "`product`")
@Data
@SqlResultSetMapping(
        name = "FindListProduct",
        classes = {
                @ConstructorResult(
                        targetClass = ProductDTO.class,
                        columns ={
                                @ColumnResult(name ="id", type = Long.class),
                                @ColumnResult(name ="name", type = String.class),
                                @ColumnResult(name ="cateId", type = Long.class),
                                @ColumnResult(name ="cateName", type = String.class),
                                @ColumnResult(name ="img", type = String.class),
                                @ColumnResult(name ="price", type = Double.class),
                                @ColumnResult(name ="quality", type = Long.class),
                                @ColumnResult(name ="enable", type = Boolean.class),
                        }
                )
        }
)
public class Product {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private Double price;
    @Column(name = "quality")
    private Long quality;
    @Column(name = "cate_id")
    private Long cateId;
    @Column(name = "img")
    private String img;
    @Column(name = "enable")
    private Boolean enable;

}
