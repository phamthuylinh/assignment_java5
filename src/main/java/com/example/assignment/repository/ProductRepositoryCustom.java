package com.example.assignment.repository;

import com.example.assignment.domain.Product;
import com.example.assignment.service.dto.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface ProductRepositoryCustom  {
Page<ProductDTO> getAllProduct(ProductDTO dto, Pageable pageable);
}
