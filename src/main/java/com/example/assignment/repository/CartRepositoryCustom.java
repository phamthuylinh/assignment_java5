package com.example.assignment.repository;

import com.example.assignment.service.dto.CartDTO;
import com.example.assignment.service.dto.CartProductDTO;
import com.example.assignment.service.dto.OrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CartRepositoryCustom {
    Page<CartDTO> getAllCart(CartDTO dto, Pageable pageable);
}
