package com.example.assignment.repository;

import com.example.assignment.domain.Cart;
import com.example.assignment.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long>, CartRepositoryCustom {

    Cart findCartByUserIdAndProductId(Long userId, Long productId);
    List<Cart> findAllByProductId(Long id);
}
