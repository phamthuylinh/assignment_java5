package com.example.assignment.repository;

import com.example.assignment.service.dto.CartDTO;
import com.example.assignment.service.dto.CartProductDTO;
import com.example.assignment.service.dto.OrderDTO;
import com.example.assignment.service.utils.CommonUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

public class CartRepositoryCustomImpl implements CartRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Page<CartDTO> getAllCart(CartDTO dto, Pageable pageable) {
        Map<String, Object> params = new HashMap<>();

        StringBuilder sql = new StringBuilder(" select c.id id, ")
                .append("c.product_id proId, ")
                .append("p.name proName, ")
                .append("c.quality quality, ")
                .append("c.price price, ")
                .append("p.img img, ")
                .append("c.user_id userId, ")
                .append("c.create_timestamp createTimestamp, ")
                .append("p.quality maxQuality ")
                .append("from cart c ")
                .append(" join product p on p.id = c.product_id ")
                .append(" where c.user_id = :userId ")
                .append(" order by c.create_timestamp ");

        params.put("userId", dto.getUserId());
        sql.append(CommonUtils.setOrderQuery(pageable.getSort()));

        return CommonUtils.getPageImpl(em, sql.toString(), params, pageable, "getListCart");

    }
}
