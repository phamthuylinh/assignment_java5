package com.example.assignment.repository;

import com.example.assignment.service.dto.OrderDTO;
import com.example.assignment.service.utils.CommonUtils;
import com.example.assignment.service.utils.DataUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

public class OrderRepositoryCustomImpl implements OrderRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Page<OrderDTO> getAllOrder(OrderDTO dto, Pageable pageable) {
        Map<String, Object> params = new HashMap<>();

        StringBuilder sql = new StringBuilder("select o.id id, ")
                .append("u.id userId, ")
                .append("u.name userName, ")
                .append("o.sum_quality sumQuality, ")
                .append("o.sum_price sumPrice, ")
                .append("o.create_timestamp createTimestamp")
                .append(" from `order` o ")
                .append(" join `user` u on o.user_id = u.id ")
                .append(" where 1=1");

        if (!DataUtils.isNullObject(dto.getUserName())) {
            sql.append(" and u.id =  :userName ");
            params.put("userName",  dto.getUserId());
        }

        if (!DataUtils.isNullObject(dto.getCreateTimestamp())) {
            sql.append(" and o.create_timestamp = :time");
            params.put("time", dto.getCreateTimestamp());
        }
        sql.append(CommonUtils.setOrderQuery(pageable.getSort()));

        return CommonUtils.getPageImpl(em, sql.toString(), params, pageable, "getListOrder");
    }

    @Override
    public Page<OrderDTO> getOrderDetail(OrderDTO dto, Pageable pageable) {
        Map<String, Object> params = new HashMap<>();

        StringBuilder sql = new StringBuilder("select od.id orderDetailId, ")
                .append("od.order_id id, ")
                .append("od.product_id productId, ")
                .append("p.name productName, ")
                .append("od.price priceEach, ")
                .append("od.quality qualityEach ")
                .append(" from order_detail od ")
                .append(" join `order` o on o.id = od.order_id ")
                .append(" join product p on p.id = od.product_id ")
                .append(" where od.order_id = :orderId");

        params.put("orderId", dto.getId());

        if(!DataUtils.isNullObject(dto.getProductName())){
            sql.append(" and p.name like :proName");
            params.put("proName", "%"+dto.getProductName()+"%");
        }

        if(!DataUtils.isNullObject(dto.getQualityEach())){
            sql.append(" and od.quality = :qualityEach");
            params.put("qualityEach", dto.getQualityEach());
        }

        if(!DataUtils.isNullObject(dto.getProductName())){
            sql.append(" and od.price = :priceEach");
            params.put("priceEach", dto.getPriceEach());
        }
        sql.append(CommonUtils.setOrderQuery(pageable.getSort()));

        return CommonUtils.getPageImpl(em, sql.toString(), params, pageable, "getListOrderDetail");

    }
}
