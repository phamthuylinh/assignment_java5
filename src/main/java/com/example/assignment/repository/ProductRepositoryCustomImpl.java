package com.example.assignment.repository;

import com.example.assignment.service.dto.ProductDTO;
import com.example.assignment.service.utils.CommonUtils;
import com.example.assignment.service.utils.DataUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

public class ProductRepositoryCustomImpl implements ProductRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Page<ProductDTO> getAllProduct(ProductDTO dto, Pageable pageable) {
        Map<String, Object> params = new HashMap<>();

        StringBuilder sql = new StringBuilder("select p.id id, ")
                .append("p.name name, ")
                .append("p.cate_id cateId, ")
                .append("c.name cateName, ")
                .append("p.img img, ")
                .append("p.price price, ")
                .append("p.quality quality, ")
                .append("p.enable enable ")
                .append("from product p ")
                .append("join category c on p.cate_id = c.id ")
                .append("where  1=1 ");

        if (!DataUtils.isNullObject(dto.getEnable())){
            sql.append(" and p.enable = :enable ");
            params.put("enable", dto.getEnable());
        }
        if (!DataUtils.isNullObject(dto.getName())) {
            sql.append(" and p.name like :proName ");
            params.put("proName", "%" + dto.getName() + "%");
        }

        if (!DataUtils.isNullObject(dto.getCateName())) {
            sql.append(" and c.name like :cateName ");
            params.put("cateName", "%" + dto.getCateName() + "%");
        }

        if (!DataUtils.isNullObject(dto.getPrice())) {
            sql.append(" and p.price = :price ");
            params.put("price", dto.getPrice());
        }

        if (!DataUtils.isNullObject(dto.getQuality())) {
            sql.append(" and p.quality = :quality ");
            params.put("quality", dto.getQuality());
        }

        sql.append(CommonUtils.setOrderQuery(pageable.getSort()));

        return CommonUtils.getPageImpl(em, sql.toString(), params, pageable, "FindListProduct");
    }
}
