package com.example.assignment.repository;

import com.example.assignment.service.dto.OrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderRepositoryCustom {
    Page<OrderDTO> getAllOrder(OrderDTO dto, Pageable pageable);
    Page<OrderDTO> getOrderDetail(OrderDTO dto, Pageable pageable);
}
