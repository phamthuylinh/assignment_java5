package com.example.assignment.repository;

import com.example.assignment.domain.Role;
import com.example.assignment.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByLogin(String login);
    User findUserByEmail(String email);
    User findUserByLoginAndPassword(String login, String password);

}
