package com.example.assignment.repository;

import com.example.assignment.domain.Cart;
import com.example.assignment.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
