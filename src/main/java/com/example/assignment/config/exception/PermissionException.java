package com.example.assignment.config.exception;

import com.example.assignment.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class PermissionException extends ServerException {
    public PermissionException(ErrorCode errorCode, ErrorCode.Params... params) {
        super(errorCode, HttpStatus.UNAUTHORIZED, params);
    }
}
