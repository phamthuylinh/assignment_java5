package com.example.assignment.config.exception;

import com.example.assignment.config.ErrorCode;

public class UserNotFoundException extends NotFoundException{
    public UserNotFoundException(){
        super(ErrorCode.Params.USER);
    }
}
