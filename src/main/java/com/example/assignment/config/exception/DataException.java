package com.example.assignment.config.exception;

import com.example.assignment.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class DataException extends ServerException {
    public DataException(ErrorCode errorCode, ErrorCode.Params... params) {
        super(errorCode, HttpStatus.BAD_REQUEST, params);
    }
}
