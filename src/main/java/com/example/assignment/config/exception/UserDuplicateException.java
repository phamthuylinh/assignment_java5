package com.example.assignment.config.exception;

import com.example.assignment.config.ErrorCode;

public class UserDuplicateException extends NotFoundException{
    public UserDuplicateException(){
        super(ErrorCode.Params.USER);
    }
}
