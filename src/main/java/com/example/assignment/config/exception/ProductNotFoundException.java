package com.example.assignment.config.exception;

import com.example.assignment.config.ErrorCode;

public class ProductNotFoundException extends NotFoundException{
    public ProductNotFoundException(){
        super(ErrorCode.Params.PRODUCT);
    }
}
