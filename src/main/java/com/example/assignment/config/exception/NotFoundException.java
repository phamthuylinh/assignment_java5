package com.example.assignment.config.exception;

import com.example.assignment.config.ErrorCode;

public class NotFoundException extends DataException {
    public NotFoundException() {
        super(ErrorCode.NOT_FOUND);
    }

    public NotFoundException(ErrorCode.Params... params) {
        super(ErrorCode.NOT_FOUND, params);
    }
}
