package com.example.assignment.config.exception;

import com.example.assignment.config.ErrorCode;

public class PermissionDeniedException extends PermissionException {
    public PermissionDeniedException() {
        super(ErrorCode.PERMISSION_DENIED);
    }
}
