package com.example.assignment.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Application constants.
 */
public final class Constants {

    private Constants() {
    }

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String SUCCESS_CODE = "0";
    public static final String SUCCESS_MSG = "SUCCESS";
    public static final String SUCCESS_URI_LINK = "SUCCESS";
    public static final String FAIL_CODE = "1";
    public static final String FAIL_MSG = "FAIL";
    public static final String FAIL_URI_LINK = "FAIL";
    public static final String TREE_ROOT_NAME = "Labels";
    public static final Long TREE_ROOT_KEY = -1L;
    public static final String CONFIG_MAIL = "EMAILS";
    public static final String MAIL_ADS = "MAIL_ADS";
    public static final String LAST_LOGIN_WARNING = "LAST_LOGIN_WARNING";
    public static final String DEFAULT_CONSENSUS_PERCENT = "DEFAULT_CONSENSUS_PERCENT";
    public static final String NUMBER_HITS_BLOCK = "NUMBER_HITS_BLOCK";
    public static final String DEFAULT_EXCEL_COL_WIDTH = "DEFAULT_EXCEL_COL_WIDTH";
    public static final String CHARGE_PERCENT = "CHARGE_PERCENT";
    public static final int RATING_MIN = 0;
    public static final int RATING_DEFAULT = 50;
    public static final int RATING_MAX = 100;
    public static final int DATE_DIFF = 10;
    public static final int USER_LIMIT = 5;
    public static final String HIT_ID = "image_id";
    public static final int DEFAULT_NUMBER_REVIEW = 20;
    public static final int FLOW_NUMBER_REVIEW = 2;
    public static final int FLOW_NUMBER_ANNOTATE = 1;
    public static final String REQUESTOR = "Requestor";
    public static final String MAIL_PREFIX = "MAIL_";

    public enum TASK_TYPES {
        IMAGE_CLASSIFICATION,
        IMAGE_OBJECT_DETECTION,
        AUDIO_TO_TEXT,
        IMAGE_OCR,
        IMAGE_OCR_SINGLE_VALIDATION,
        AUDIO_ANALYSIS,
        VIDEO_OBJECT_DETECTION,
        AUDIO_SEGMENTATION,
        TEXT_NER,
        TEXT_CLASSIFICATION,
        TEXT_GENERATION,
        DOCUMENT_SUMMARIZATION,
        DATA_COLLECTION,
    }

    public enum UPLOAD_OPTIONS {
        S3,
        LOCAL
    }

    public static final String UPLOAD_SUFFIX = "_UPLOAD_CONFIG";

    @Getter
    public enum FILE_EXT {
        //upload
        TXT("txt"),
        ZIP("zip"),
        XLSX("xlsx"),
        XLS("xls"),

        // audio
        MP3("mp3"),
        MP4("mp4"),
        WAV("wav"),

        // vid
        MOV("mov"),

        // img
        PNG("png"),
        JPG("jpg"),
        JPEG("jpeg"),

        // text
        DOCX("docx"),

        JSON("json");

        FILE_EXT(String extension) {
            this.extension = extension;
        }

        private static final List<String> LIST_NAME =
            Arrays.stream(FILE_EXT.values()).map(FILE_EXT::name).collect(Collectors.toList());

        private final String extension;

        public String toExtension() {
            return "." + this.getExtension();
        }

        public static FILE_EXT getValidEnum(String name) {
            name = name.toUpperCase();
            return LIST_NAME.contains(name) ? FILE_EXT.valueOf(name) : null;
        }
    }

    public enum FILE_TYPES {
        JSON_TYPE,
        EXCEL_TYPE
    }

    public enum STATUS {
        NOT_DONE,
        DONE,
        REJECTED,
        ACCEPTED,
        INVITED,
    }

    public enum HIT_USER_STATUS {
        NOT_DONE,
        DONE,
        REVIEW_READY,
        FIXING,
        DISABLED,
        NEED_ACTION
    }

    public enum PROJECT_USER_STATUS {
        REJECTED,
        INVITED,
        INVITED_TEST,
        DOING
    }

    public enum TRANSACTION_PAYMENT_STATUS {
        REQUESTED,
        APPROVED,
        REJECTED,
        CANCEL
    }

    public enum EVALUATION {
        NONE,
        CORRECT,
        INCORRECT,
        REVIEW_READY
    }

    public enum QUESTION_TYPE {
        MULTIPLE_CHOICE,
        SINGLE_CHOICE,
        TITLE
    }

    public enum TEST_ROLE {
        OWNER,
        SHARER
    }

    public enum TEST_TYPE{
        THEORY,
        PRACTICE
    }

    @Getter
    public enum PROJECT_STATUS {
        NEW("NEW"),
        OPEN("OPEN"),
        DOING("DOING"),
        CLOSED("CLOSED"),
        AWAITING_APPROVAL("AWAITING_APPROVAL");

        private String value;

        PROJECT_STATUS(String value) {
            this.value = value;
        }
    }

    public enum PROJECT_TYPES {
        IMAGE_TYPE,
        TEXT_TYPE,
        AUDIO_TYPE,
        VIDEO_TYPE
    }

    public enum FILTER_PROJECT {
        INVITE,
        REGISTERED,
        NOT_REGISTER,
    }


    @Getter
    public enum BOOLEAN_VALUE {
        TRUE(1),
        FALSE(0);

        private int value;

        BOOLEAN_VALUE(int value) {
            this.value = value;
        }
    }

    public enum ROLE_PROJECT {
        ADMIN,
        PROJECT_ASSISTANT,
        CONTRIBUTOR,
        REVIEWER,
        SAMPLE_REVIEWER,
        SUPERVISOR
    }

    public enum PROFILE_COMPLETION {
        BASE_INFO,
        LANGUAGES,
        EXPERIENCE,
        EDUCATION,
        BANK_INFO,
        DEAL,
        DOCUMENTS,
        WORK_LOCATIONS
    }

    public enum REPORT_OPTIONS {
        DAILY,
        MONTHLY,
        QUARTERLY,
        YEARLY,
        TODAY,
        THIS_WEEK
    }

    public enum WORK_TYPE {
        TAGON,
        OTHERS
    }

    public enum WORK_LOCATIONS {
        REMOTE,
        ON_SITE
    }

    public enum PROJECT_TARGET {
        ANNOTATE,
        VALIDATE
    }

    public enum SUPPORT_STATUS {
        OPEN,
        RESOLVED
    }

    public enum OPERATORS {
        EQUAL_TO,
        LESS_THAN,
        GREATER_THAN,
        NOT_EQUAL_TO,
        GREATER_OR_EQUAL,
        LESS_OR_EQUAL
    }

    public enum CRITERIA {
        VERIFIED,
        SEX,
        AGE,
        LANGUAGE,
        RATING_IMAGE,
        RATING_VIDEO,
        RATING_AUDIO,
        RATING_TEXT,
        SKILL
    }

    public enum SUPPORT_TYPE {
        REPORT,
        PROJECT
    }

    @Getter
    public enum REDIS_KEY {
        KEY("invitation"),
        KEY_LINK("invitation_link"),
        NOTIFY_FIXING("notify_fixing"),
        FIXING_DONE("fixing_done");

        private String value;

        REDIS_KEY(String value) {
            this.value = value;
        }
    }

    public static class RedisKey {
        public static class Hash {
            // hask key of project, var is project id and project createduser
            // Ex: project:16:10
            public static final String PROJECT_KEY_TEMPLATE = "project:%s:%s";

            public static class FieldKey {
                public static final String TIME_EXECUTE_REDO = "time-execute-redo";
            }
        }

        public static class Set {
            public static final String PROJECT_USER_ACCEPTED_REDO = "project:%s:%s:user-accepted-redo";
        }
    }

    @Getter
    public enum EMAIL_TEMPLATE {
        ADMIN_REVIEW_DONE_NOTI("mail/requestorReviewDone",
            new Variable("email.review.done.requestor.title",
                Arrays.asList("url", "projectId", "projectName"))),
        ADMIN_REVIEW_FAIL_NOTI("mail/requestorReviewFail",
            new Variable("email.review.fail.requestor.title",
                Arrays.asList("url", "projectId", "projectName", "numberOfFail", "totalAnnotator"))),
        ACTIVATION_ACCOUNT("mail/activationEmail",
            new Variable("email.register.confirm.title",
                Collections.singletonList("user"))),
        VERIFY_ACCOUNT("mail/verifyAccountEmail",
            new Variable("email.verify.confirm.title",
                Collections.singletonList("user"))),
        RESET_PASSWORD("mail/passwordResetEmail",
            new Variable("email.reset.title",
                Collections.singletonList("user"))),
        INVITE_EMAIL("mail/inviteEmail",
            new Variable("email.invite.title",
                Arrays.asList("userName", "project", "inviteLink", "deadline", "numberWI", "role", "taskType", "title", "numberWIKey"))),
        INVITE_BY_EMAIL("mail/inviteUserByEmail",
            new Variable("email.invite.title",
                Arrays.asList("projectName", "createdUser", "inviteLink"))),
        ACCEPT_EMAIL("mail/acceptEmail",
            new Variable("email.accept.title",
                Arrays.asList("userName", "project", "inviteLink", "deadline", "numberWI", "bid", "taskType"))),
        REJECT_EMAIL("mail/rejectedEmail",
            new Variable("email.reject.title",
                Arrays.asList("userName", "project", "inviteLink", "deadline", "numberWI", "bid", "taskType"))),
        REVIEWED_EMAIL("mail/hitReviewedFinal",
            new Variable("email.review.title",
                Arrays.asList("projectName", "result", "note", "qualityRequired", "qualityRequiredDouble", "bidAmount", "earned", "resultsUrl", "totalHit"))),
        REVIEW_READY("mail/reviewReadyNotice",
            new Variable("email.reviewReadyNotice.title",
                Arrays.asList("project", "username"))),
        UPLOAD_DATA("mail/uploadDataEmail",
            new Variable("email.uploadData.title",
                Arrays.asList("project", "uploadResponse", "isZip"))),
        UPLOAD_DATA_FAIL("mail/uploadDataEmailFail",
            new Variable("email.uploadData.title",
                Arrays.asList("project", "uploadResponse"))),
        REVIEW_TEST_DONE("mail/reviewReadyTestNotice",
            new Variable("email.reviewReadyTestNotice.title",
                Arrays.asList("username", "project", "link"))),
        ACCEPT_REQUEST_MONEY("mail/requestAcceptedMail",
            new Variable("email.transaction.accepted.title",
                Arrays.asList("loginUser", "loginApprover", "amount", "transFee", "approveDate", "requestDate", "balance", "transCode", "tagOnAccount", "title", "content"))),
        REJECT_REQUEST_MONEY("mail/requestRejectedMail",
            new Variable("email.transaction.rejected.title",
                Arrays.asList("loginUser", "loginApprover", "rejectedReason", "requestDate", "amount", "transFee", "transCode", "tagOnAccount", "title", "content"))),
        OTP_EMAIL("mail/requestOTPMail",
            new Variable("email.otp.title",
                Arrays.asList("loginUser", "otp", "exp"))),
        ANN_PROJECT_CLOSED_EMAIL("mail/hitUserCloseProject",
            new Variable("email.review.close.title",
                Arrays.asList("projectName", "countHit", "bidAmount", "earned"))),
        REQ_PROJECT_CLOSED_EMAIL("mail/closedProjectEmail",
            new Variable("email.closed.title",
                Arrays.asList("firstName", "projectName", "createdDate", "deadline", "countHits", "payAnn", "paySys",
                    "hold", "refund", "projectDetailLink", "countTotalHits", "hitUnAnnotated", "totalPayment", "countHitsDone"))),
        START_REVIEWED("mail/startReviewed",
            new Variable("reviewed.subtitle",
                Arrays.asList("username", "numberWI", "percent", "percentNumberWI", "qualityRequirement"))),
        LOCK_USER("mail/lockUser",
            new Variable("lock.subtitle",
                Collections.singletonList("projectName"))),
        UNLOCK_USER("mail/unlockUser",
            new Variable("unlock.subtitle",
                Collections.singletonList("projectName"))),
        FIXING_TO_REDO("mail/redoHit",
            new Variable("redo.subtitle",
                Arrays.asList("projectName", "deadline", "dateFixing"))),
        NOTIFY_FIXING("mail/notifyFixing",
            new Variable("notify.subtitle",
                Arrays.asList("projectName", "projectType", "description", "bidAmount", "WISubmit", "labelingQuality", "qualityRequired", "userName", "deadline", "inviteLink", "taskType"))),
        STOP_HIRING("mail/stopHiring",
            new Variable("hiring.subtitle",
                Collections.singletonList("projectName"))),
        APPROVED_APPROVAL_PROJECT("mail/approvedApprovalProject",
            new Variable("approval.approved.subtitle",
                Collections.singletonList("projectName"))),
        REJECTED_APPROVAL_PROJECT("mail/rejectApprovalProject",
            new Variable("approval.rejected.subtitle",
                Arrays.asList("projectName", "reason"))),
        EMAIL_APPROVE_REVIEW("mail/approveReview",
            new Variable("",
                Arrays.asList("fullName", "project", "resultsUrl", "title", "content", "message", "taskType", "deadline", "amount", "keyWI", "numberWI", "key", "value", "textButton"))),
        EMAIL_NEED_REVIEW("mail/needReview",
            new Variable("email.needReview.title",
                Arrays.asList("fullName", "project", "inviteLink", "taskType", "deadline"))),
        EMAIL_DATASET_ADDITIONAL("mail/dataAdditional",
            new Variable("email.dataset.additional.title",
                Arrays.asList("fullName", "project", "inviteLink", "taskType", "deadline", "bidAmount"))),
        EMAIL_REGISTER_ACCOUNT("mail/registerAccount",
            new Variable("email.register.account.title",
                Arrays.asList("fullName"))),
        EMAIL_WAITING_LIST("mail/emailWaitingList",
            new Variable("email.request.pending.title1",
                Arrays.asList("userName", "numberOfAnnotator", "date", "requests", "inviteLink"))),
        MARKETING_PROJECTS("mail/marketingProjects",
            new Variable("email.project.marketing.title",
                Arrays.asList("userName", "inviteLink", "projects"))),
        FIXING_DONE("mail/fixingDone",
            new Variable("email.fixing.done.title1",
                Arrays.asList("userName", "projectName", "numberOfReAnnotating", "inviteLink"))),
        FINANCIAL_ANNOTATOR("mail/financialAnnotator",
            new Variable("email.financial.annotator.title",
                Arrays.asList("userName", "login", "currentMonth", "requests", "linkProject", "financeLink", "totalMoney", "waitProjectReviewMoney", "waitApproveMoney"))),
        FINANCIAL_REQUESTER("mail/financialRequester",
            new Variable("email.financial.requester.title",
                Arrays.asList("userName", "login", "currentMonth", "requests", "linkProject", "financeLink", "totalMoney", "sumHold", "balance"))),
        TRANSACTION_EVENT("mail/transactionEvent",
            new Variable("event.tet.subtitle",
                Arrays.asList("rank", "money"))),
        ERROR_WS_EVENT("mail/errorWSEvent",
            new Variable("event.error.subtitle",
                Arrays.asList("url", "action", "username"))),
        REFERRAL_ACCOUNT("mail/referralAccount",
            new Variable("event.tet.referral.subtitle",
                Collections.singletonList("username"))),
        COMMENT_NOTICE("mail/commentNotiMail",
            new Variable("email.comment.title",
                Arrays.asList("projectName", "text", "resultsUrl", "chatLogs"))),
        EXTEND_HIRING_NOTICE("mail/extendDeadlineMail",
            new Variable("email.extend.deadline.title",
                Arrays.asList("projectName", "resultsUrl"))),
        REQUEST_VERIFIED("mail/requestVerified",
            new Variable("event.request.subtitle",
                Arrays.asList("username", "UserManagerUrl"))),
        AFTER_ACTIVATED("mail/afterActivateEmail",
            new Variable("email.afterVerify.title",
                Arrays.asList("username","content"))),
        ANN_REFUND_MAIL("mail/hitRefundProjectAnn",
            new Variable("email.annRefundMail.subtitle",
                Arrays.asList("projectName", "user", "refund", "totalAfter", "totalBefore", "countHits", "countHitsError", "resultsUrl")));

        EMAIL_TEMPLATE(String resourcePath, Variable variable) {
            this.resourcePath = resourcePath;
            this.variable = variable;
        }

        private Variable variable;
        private String resourcePath;

        @AllArgsConstructor
        @Getter
        @Setter
        public static class Variable {
            private String subject;
            private List<String> params;
        }
    }

    @Getter
    public enum TASK_TYPE_ROUTE_MAPPER {
        VIDEO_OBJECT_DETECTION("video-segmentation"),
        IMAGE_CLASSIFICATION("classification-photos"),
        IMAGE_OBJECT_DETECTION("segmentation-photos"),
        IMAGE_OCR("ocr-photos"),
        AUDIO_TO_TEXT("speech-to-text"),
        AUDIO_SEGMENTATION("audio-segmentation"),
        TEXT_NER("text-ner"),
        IMAGE_OCR_SINGLE_VALIDATION("ocr-photos-validation"),
        AUDIO_ANALYSIS("speech-analysis"),
        TEXT_CLASSIFICATION("text-classification"),
        TEXT_GENERATION("text-generation"),
        DOCUMENT_SUMMARIZATION("document-summarization");

        private final String value;

        TASK_TYPE_ROUTE_MAPPER(String value) {
            this.value = value;
        }
    }

    public static final String WS_DES_BASE = "/topic/broadcast";

    @Getter
    public enum WS_TOPIC {
        FORUM("/forum/"),
        FORUM_MEMBERS("/forum-members/"),
        NOTIFICATION("/notifications/");

        private final String value;

        WS_TOPIC(String value) {
            this.value = value;
        }
    }

    public enum TRANSACTION_TYPE {
        // annotator(user_id) receive amount from project for items completed, sys_admin(approver_id) has to approve
        INCOME,
        // project_admin(user_id) paid for dataset in project, +amount to sys_admin(receiver_id, approver_id) admin2
        PAYMENT,
        // sys admin(user_id, approver_id) +amount to wallet of project_admin(receiver_id)
        ADD_TO_WALLET,
        //  +amount to wallet of project_admin(receiver_id)
        REFUND,
        // project admin request deposit
        DEPOSIT,
        // on hold
        ON_HOLD
    }

    public enum CLONE_OPTIONS {
        DATA,
        RULES,
        ANNOTATORS,
        LABELS,
        HAS_TEST
    }

    public enum ASSIGN_TYPE {
        ADD,
        SUBTRACT
    }

    public enum RATING_HISTORY_ACTIONS {
        TEST_FAILED,
        FINISH_PROJECT,
        REMOVED,
        EVALUATION_PROJECT
    }

    public enum NOTIFICATION_TYPE {
        SYSTEM,
        TRANSACTION,
        PROJECT,
        REPORT,
        ACCOUNT,
        TENANT,
        TENANT_REQUEST
    }

    public enum TENANT_REQUEST_TYPE {
        CREATED,
        APPROVED,
        REJECTED
    }

    public enum PAYMENT_STATUS {
        NONE,
        DONE
    }

    public enum PROJECT_FLOW_CODE {
        ANNOTATOR,
        REVIEWER
    }

    public enum ANNOTATOR_STAT {
        INCORRECT_ITEMS,
        SUBMITTED_ITEMS,
        KICKED_NUMBER,
        BATCH_ITEMS,
        BATCH_TIMEOUT,
    }


    @Getter
    public enum PROJECT_CONFIG_JSON_KEY {
        IS_REVIEW("isReview", true),
        REVIEW_PERCENT("percent", true),
        BATCH_MAX_ITEM("batchMaxItem", true),
        BATCH_TIMEOUT("batchTimeout", true),
        SAMPLES_TIME("samplesTime", true),
        NUMBER_OF_SAMPLES("numberOfSamples", true),
        QUALITY_REQUIREMENT("qualityRequirement", false),
        ASSIGN_TYPE("assignType", false),
        MAX_HIT_RECEIVE("maxHitReceive", false),
        TOOL_REVIEW_URL("toolReviewUrl", false),
        TOOL_URL("toolUrl", false),
        ANNOTATE_BY_DATASET("annotateByDataset", false),
        REVIEW_BY_DATASET("reviewByDataset", false);

        private final String value;
        private final Boolean required;

        PROJECT_CONFIG_JSON_KEY(String value, boolean required) {
            this.value = value;
            this.required = required;
        }

        public static List<String> getRequiredValues() {
            return Arrays.stream(PROJECT_CONFIG_JSON_KEY.values())
                .filter(PROJECT_CONFIG_JSON_KEY::getRequired)
                .map(en -> en.value)
                .collect(Collectors.toList());
        }
    }

    public enum MESSAGE_TYPE {
        ADD_NEW,
        UPDATE,
        DELETE,
        REACT,
        PIN_TOP
    }

    public enum REVIEW_RESULT_KEEP_OPT {
        REVIEWER,
        REVIEWER_RESULT,
        EVALUATION
    }

    //Tenant
    public static final String TENANT_ERR_01 = "TENANT_ERR_01";
    public static final String TENANT_CODE_MUST_BE_NOT_NULL = "Tenant code must be not null";
    public static final String TENANT_ERR_02 = "TENANT_ERR_02";
    public static final String TENANT_NAME_MUST_BE_NOT_NULL = "Tenant name must be not null";
    public static final String TENANT_ERR_03 = "TENANT_ERR_03";
    public static final String TAX_CODE_MUST_BE_NOT_NULL = "Tax code name must be not null";
    public static final String TENANT_ERR_04 = "TENANT_ERR_04";
    public static final String EMAIL_MUST_BE_NOT_NULL = "Email must be not null";
    public static final String TENANT_ERR_05 = "TENANT_ERR_05";
    public static final String PHONE_NUMBER_MUST_BE_NOT_NULL = "Phone number must be not null";
    public static final String TENANT_ERR_06 = "TENANT_ERR_06";
    public static final String TENANT_MUST_BE_NOT_DUPLICATE = "Tenant must be not duplicate";
    public static final String TENANT_ERR_07 = "TENANT_ERR_07";
    public static final String TENANT_CODE_DO_NOT_EXITS = "Tenant code do not exits";
    public static final String TENANT_ERR_08 = "TENANT_ERR_08";
    public static final String TAX_CODE_MUST_BE_NOT_DUPLICATE = "Tax code must be not duplicate";
    public static final String TENANT_ERR_09 = "TENANT_ERR_09";
    public static final String TENANT_NAME_MUST_BE_NOT_DUPLICATE = "Tenant name must be not duplicate";
    public static final String TENANT_ERR_10 = "TENANT_ERR_10";
    public static final String TENANT_BEING_USED = "Tenant is being used in user table";

    //Partner
    public static final String PARTNER_ERR_01 = "PARTNER_ERR_01";
    public static final String PARTNER_CODE_MUST_BE_NOT_NULL = "Partner code must be not null";
    public static final String PARTNER_ERR_02 = "PARTNER_ERR_02";
    public static final String PARTNER_NAME_MUST_BE_NOT_NULL = "Partner name must be not null";
    public static final String PARTNER_ERR_03 = "PARTNER_ERR_03";
    public static final String PARTNER_ERR_04 = "PARTNER_ERR_04";
    public static final String PARTNER_ERR_05 = "PARTNER_ERR_05";
    public static final String PARTNER_ERR_06 = "PARTNER_ERR_06";
    public static final String PARTNER_MUST_BE_NOT_DUPLICATE = "Partner must be not duplicate";
    public static final String PARTNER_ERR_07 = "PARTNER_ERR_07";
    public static final String PARTNER_CODE_DO_NOT_EXITS = "Partner code do not exits";

    //label
    public static final String LABEL_ERR_01 = "LABEL_ERR_01";
    public static final String LABEL_EXIST_CYCLE = "Exist cycle in the label list.";
    public static final String LABEL_ERR_02 = "LABEL_ERR_02";
    public static final String LABEL_MUST_BE_NOT_DUPLICATE = "Label must be not duplicate";

    //config
    public static final String CONFIG_ERR_01 = "CONFIG_ERR_01";
    public static final String CONFIG_CODE_MUST_BE_NOT_NULL = "Config code must be not null";
    public static final String CONFIG_ERR_02 = "CONFIG_ERR_02";
    public static final String CONFIG_NAME_MUST_BE_NOT_NULL = "Config name must be not null";
    public static final String CONFIG_ERR_03 = "CONFIG_ERR_03";
    public static final String CONFIG_MUST_BE_NOT_DUPLICATE = "Config must be not duplicate";
    public static final String CONFIG_ERR_04 = "CONFIG_ERR_04";
    public static final String CONFIG_CODE_DO_NOT_EXITS = "Config code do not exits";

    // profile
    public static final String BASE_INFO = "BASE_INFO";
    public static final String LANGUAGES = "LANGUAGES";
    public static final String EXPERIENCE = "EXPERIENCE";
    public static final String EDUCATION = "EDUCATION";
    public static final String BANK_INFO = "BANK_INFO";
    public static final String DEAL = "DEAL";
    public static final String DOCUMENTS = "DOCUMENTS";

    // validate
    public static final String DATA_TOO_LONG = "Input data too long";

    // source
    public static final String WEBSITE = "website";
    public static final String FACEBOOK = "facebook";
    public static final String REFERRAL = "referral";
    public static final String OTHER = "other";

    //file
    public static final String EXCEL_TYPE = ".xlsx";
    public static final String TEXT_TYPE = ".txt";

    //rating history
    public static final String RATING_ERR_01 = "RATING_ERR_01";
    public static final String RATING_ERR_02 = "RATING_ERR_02";
    public static final String RATING_ERR_03 = "RATING_ERR_03";
    public static final String RATING_PROJECT_ID_MUST_NOT_BE_NULL = "PROJECT_ID_MUST_NOT_BE_NULL";
    public static final String RATING_USER_ID_MUST_NOT_BE_NULL = "USER_ID_MUST_NOT_BE_NULL";
    public static final String RATING_ACTION_MUST_NOT_BE_NULL = "ACTION_MUST_NOT_BE_NULL";

    // notification
    public static final String NOTIFICATIONS_KEEP = "NOTIFICATIONS_KEEP";

    // MIN_AMOUNT_PER_ITEM
    public static final String MIN_AMOUNT_PER_ITEM = "MIN_AMOUNT_PER_ITEM";

    public enum ACTIVITY_TYPE {
        DATA,
        REGISTER,
        STATUS,
        REVIEW,
        INVITE,
        ANNOTATE,
        INFO,
        COST,
        USER
    }

    // activities template key (table config)
    public static class ActivityTemplate {
        public static final String ACT_PROJECT_CREATED = "ACT_PROJECT_CREATED";
        public static final String ACT_UPLOAD_FILE = "ACT_UPLOAD_FILE";
        public static final String ACT_PROJECT_ACCEPT = "ACT_PROJECT_ACCEPT";
        public static final String ACT_PROJECT_REJECT = "ACT_PROJECT_REJECT";
        public static final String ACT_PROJECT_HIRING = "ACT_PROJECT_HIRING";
        public static final String ACT_PROJECT_HIRING_OFF = "ACT_PROJECT_HIRING_OFF";
        public static final String ACT_PROJECT_UNLOCK_USER = "ACT_PROJECT_UNLOCK_USER";
        public static final String ACT_PROJECT_LOCK_USER = "ACT_PROJECT_LOCK_USER";
        public static final String ACT_INVITE_USER = "ACT_INVITE_USER";
        public static final String ACT_PROJECT_LABEL_UPDATED = "ACT_PROJECT_LABEL_UPDATED";
        public static final String ACT_PROJECT_CLOSED = "ACT_PROJECT_CLOSED";
        public static final String ACT_PROJECT_UPLOAD = "ACT_PROJECT_UPLOAD";
        public static final String ACT_PROJECT_DELETE_DATASET = "ACT_PROJECT_DELETE_DATASET";
        public static final String ACT_PROJECT_REFUND_ITEM = "ACT_PROJECT_REFUND_ITEM";
        public static final String ACT_PROJECT_REFUND_CLOSE = "ACT_PROJECT_REFUND_CLOSE";
        public static final String ACT_PROJECT_HOLD = "ACT_PROJECT_HOLD";
        public static final String ACT_PROJECT_ON_REVIEWING = "ACT_PROJECT_ON_REVIEWING";
        public static final String ACT_PROJECT_OFF_REVIEWING = "ACT_PROJECT_OFF_REVIEWING";
        public static final String ACT_PROJECT_REVIEW_ALLOW_PASSED = "ACT_PROJECT_REVIEW_ALLOW_PASSED";
        public static final String ACT_PROJECT_REVIEW_APPLY_FIXING = "ACT_PROJECT_REVIEW_APPLY_FIXING";
        public static final String ACT_PROJECT_REVIEW_APPLY_REDO = "ACT_PROJECT_REVIEW_APPLY_REDO";
        public static final String ACT_PROJECT_PAYMENT = "ACT_PROJECT_PAYMENT";
        public static final String ACT_PROJECT_ACCEPT_INVITE = "ACT_PROJECT_ACCEPT_INVITE";
        public static final String ACT_PROJECT_REJECT_INVITE = "ACT_PROJECT_REJECT_INVITE";
        public static final String ACT_PROJECT_GET_HIT_ANNOTATE = "ACT_PROJECT_GET_HIT_ANNOTATE";
        public static final String ACT_PROJECT_GET_HIT_REVIEW = "ACT_PROJECT_GET_HIT_REVIEW";
        public static final String ACT_PROJECT_HIT_ANNOTATE_TIMEOUT = "ACT_PROJECT_HIT_ANNOTATE_TIMEOUT";
        public static final String ACT_PROJECT_HIT_REVIEW_TIMEOUT = "ACT_PROJECT_HIT_ANNOTATE_TIMEOUT";
        public static final String ACT_PROJECT_DATASET_CREATED = "ACT_PROJECT_DATASET_CREATED";
        public static final String ACT_PROJECT_USER_BID = "ACT_PROJECT_USER_BID";
        public static final String ACT_PROJECT_CLOSED_ERR = "ACT_PROJECT_CLOSED_ERR";
        public static final String ACT_PROJECT_EXTEND_DEADLINE = "ACT_PROJECT_EXTEND_DEADLINE";
        public static final String ACT_PROJECT_RE_REVIEW = "ACT_PROJECT_RE_REVIEW";
    }

    // notification template key (table config)
    public static class NotificationTemplate {
        public static final String TEMP_PROJECT_INVITE = "TEMP_PROJECT_INVITE";
        public static final String TEMP_PROJECT_REPLY_INVITE = "TEMP_PROJECT_REPLY_INVITE";
        public static final String TEMP_PROJECT_REPLY_REJECT = "TEMP_PROJECT_REPLY_REJECT";
        public static final String TEMP_ADD_USER_ROLE = "TEMP_ADD_USER_ROLE";
        public static final String TEMP_REMOVE_USER = "TEMP_REMOVE_USER";
        public static final String TEMP_START_PROJECT = "TEMP_START_PROJECT";
        public static final String TEMP_CLOSE_PROJECT = "TEMP_CLOSE_PROJECT";
        public static final String TEMP_APPROVE_TRANSACTION = "TEMP_APPROVE_TRANSACTION";
        public static final String TEMP_REJECT_TRANSACTION = "TEMP_REJECT_TRANSACTION";
        public static final String TEMP_PROJECT_PAYMENT = "TEMP_PROJECT_PAYMENT";
        public static final String TEMP_REPORT_RESOLVE = "TEMP_REPORT_RESOLVE";
        public static final String TEMP_PROJECT_PUBLISH = "TEMP_PROJECT_PUBLISH";
        public static final String TEMP_CREATE_TRANSACTION = "TEMP_CREATE_TRANSACTION";
        public static final String TEMP_REPORT_REPLY = "TEMP_REPORT_REPLY";
        public static final String TEMP_REPORT_CREATED = "TEMP_REPORT_CREATED";
        public static final String TEMP_ACCOUNT_VERIFY = "TEMP_ACCOUNT_VERIFY";
        public static final String TEMP_ACCOUNT_RESET_PASSWORD = "TEMP_ACCOUNT_RESET_PASSWORD";
        public static final String TEMP_TENANT_REQUEST_CREATED = "TEMP_TENANT_REQUEST_CREATED";
        public static final String TEMP_TENANT_REQUEST_APPROVED = "TEMP_TENANT_REQUEST_APPROVED";
        public static final String TEMP_PROJECT_ACCEPT = "TEMP_PROJECT_ACCEPT";
        public static final String TEMP_ACCOUNT_REGISTER = "TEMP_ACCOUNT_REGISTER";
        public static final String TEMP_ACCOUNT_CREATED = "TEMP_ACCOUNT_CREATED";
        public static final String TEMP_TENANT_CREATED = "TEMP_TENANT_CREATED";
        public static final String TEMP_TENANT_UPDATED = "TEMP_TENANT_UPDATED";
        public static final String TEMP_TENANT_DELETED = "TEMP_TENANT_DELETED";
        public static final String TEMP_ACCOUNT_DELETED = "TEMP_ACCOUNT_DELETED";
        public static final String TEMP_ACCOUNT_LOCKED = "TEMP_ACCOUNT_LOCKED";
        public static final String TEMP_ACCOUNT_UNLOCKED = "TEMP_ACCOUNT_UNLOCKED";
        public static final String TEMP_ACCOUNT_AUTHOR_UPDATE = "TEMP_ACCOUNT_AUTHOR_UPDATE";
        public static final String TEMP_ACCOUNT_TENANT_UPDATE = "TEMP_ACCOUNT_TENANT_UPDATE";
        public static final String TEMP_PROJECT_USER_LEAVE = "TEMP_PROJECT_USER_LEAVE";
        public static final String TEMP_PROJECT_ADMIN_DEPOSIT = "TEMP_PROJECT_ADMIN_DEPOSIT";
        public static final String TEMP_ANNOTATE_FIXING = "TEMP_ANNOTATE_FIXING";
        public static final String TEMP_ANNOTATE_SUBMIT_FOR_REVIEW = "TEMP_ANNOTATE_SUBMIT_FOR_REVIEW";
        public static final String TEMP_APPROVE_DEPOSIT = "TEMP_APPROVE_DEPOSIT";
        public static final String TEMP_REJECT_DEPOSIT = "TEMP_REJECT_DEPOSIT";
        public static final String TEMP_PROJECT_READY_REVIEW = "TEMP_PROJECT_READY_REVIEW";
        public static final String TEMP_TEST_REVIEW_DONE = "TEMP_TEST_REVIEW_DONE";
        public static final String TEMP_TEST_SUBMIT = "TEMP_TEST_SUBMIT";
        public static final String TEMP_TRANSACTION_REQUEST_CREATED = "TEMP_TRANSACTION_REQUEST_CREATED";
        public static final String TEMP_CREATE_TOPIC = "TEMP_CREATE_TOPIC";
        public static final String TEMP_CLOSE_PROJECT_ERR = "TEMP_CLOSE_PROJECT_ERR";
        public static final String TEMP_FORUM_MENTIONS = "TEMP_FORUM_MENTIONS";
        public static final String TEMP_FORUM_REPLY = "TEMP_FORUM_REPLY";
        public static final String TEMP_FORCE_TURN_OFF_HIRING = "TEMP_FORCE_TURN_OFF_HIRING";
    }

    public static class Message {
        public static final String ADD_WALLET = "message.transaction.addWallet";
        public static final String ADD_WALLET_EMAIL_TITLE = "message.transaction.addWallet.email.title";
        public static final String ADD_WALLET_EMAIL_CONTENT = "message.transaction.addWallet.email.content";
        public static final String CLOSE_PROJECT_NOTE = "message.transaction.note.closeProject";
        public static final String ITEM_REFUND_NOTE = "message.transaction.note.refundItem";
        public static final String CLOSE_PROJECT_REFUND_NOTE = "message.transaction.note.refundCloseProject";
        public static final String ANNOTATOR_INCOME_NOTE = "message.transaction.note.annotatorIncome";
        public static final String HOLD_MONEY_PROJECT = "message.transaction.note.itemsHold";
    }

    public enum HIT_REVIEWED_RESULT {
        PASSED,
        FAILED
    }

    public enum HIT_REVIEW_ACTION {
        NONE,
        FIXING,
        REDO,
        ALLOW_PASSED,
        RE_REVIEW
    }

    @Getter
    public enum PROJECT_DETAIL_TABS {
        OVERVIEW("overview"),
        PERSONNEL("personnel"),
        DATA("data"),
        LABEL("label"),
        SETTING("setting"),
        REVIEW_RESULT("reviewResult"),
        CONDITION("condition");

        private final String value;

        PROJECT_DETAIL_TABS(String value) {
            this.value = value;
        }
    }

    public enum TENANT_POSITION {
        CEO,
        CTO,
        TECHNICAL_LEADER,
        SALE,
        DEVELOPER,
        OTHER
    }

    public enum TENANT_TYPE {
        INDIVIDUAL,
        ENTERPRISE
    }

    public static final String LABEL_UNCHECK_PROJECTS = "LABEL_UNCHECK_PROJECTS";
    public static final String TEST_RESULTS_UNCHECK_PROJECTS = "TEST_RESULTS_UNCHECK_PROJECTS";
    public static final String DATA_UNCHECK_PROJECTS = "DATA_UNCHECK_PROJECTS";

    public enum LABEL_TOOL {
        POLYGON,
        RECT,
        KEY_POINTS,
        POLYLINE,
        POINTS,
        CUBOID,
        ALL
    }

    public enum STATUS_LABELING {
        LABELING,
        TESTING,
    }

    public enum EXPORT_TYPE {
        YOLO,
        COCO,
        CVAT_IMAGE,
        COCO_WITH_DATA,
        SUPERVISELY,
        SUPERVISELY_WITH_DATA,
        EXCEL,
        TEXT,
        ASR_MANIFEST,
        JSON
    }

    public enum LABEL_ATTRIBUTE_INPUT_TYPE {
        TEXT,
        SELECT,
        NUMBER,
        CHECKBOX,
        RADIO,
    }

    public static class JSON_FIELDS {
        public static class HIT_USER {
            public static final String RESULT = "result";
        }
    }

    public enum EVENT_TET {
        EVENT_TET,
        REGISTER_ACCOUNT,
        VERIFIED_ACCOUNT,
        REFERRAL_ACCOUNT,
        REGISTER_PROJECT,
        LOGIN_DAILY,
        REVIEW_PASSED,
    }
}
